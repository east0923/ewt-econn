"use strict";

const conf={
  maxOriginalPackLength:2*1024*1024 // 默认原始数据包最大长度
};

const os=require('os');
const fs=require('fs');
const net=require('net');
const zlib = require('zlib');
const crypto = require('crypto');
const EventEmitter = require('events');
const etools= require('ewt-etools');

// sixCode为0时，cmds格式编码互转
function buffer2cmds(buf) {
  if(!Buffer.isBuffer(buf)) throw new Error('buf is not Buffer');
  const str=buf.toString('ascii');
  return str.split('|');
}
function cmds2buffer(cmds){
  // 单值字符串转数组
  if(typeof cmds==='string') cmds=[cmds];
  // 不是数组throw
  if(!Array.isArray(cmds)) throw new Error('cmds is not Array');
  // 每一项目都必须是数字或字符串
  cmds.forEach((i)=>{
    const t=typeof i;
    if(t!=='number'&&t!=='string') throw new Error('cmd\'s item must be str/num');
  });
  // 反馈
  return Buffer.from(cmds.join('|'),'ascii');
}

// sixCode为1时，jr消息格式互转
function jr2buffer(json,raw){
  // 格式验证
  if(json&&!etools.typeMatch(json,['object','array'])) throw new Error('json must be Object');
  else{
    // 将json转化为流
    json=Buffer.from(JSON.stringify(json),'utf8');
  }
  if(raw &&!etools.typeMatch(json,'buffer')) throw new Error("input raw isNot Buffer");

  // 流整理
  let mixJsonRaw;
  const zeroBuffer=Buffer.alloc(1);
  // 有 json 和 raw
  if(json && raw){
    mixJsonRaw=Buffer.concat([json,zeroBuffer,raw])
  }
  // 有json
  else if(json){
    mixJsonRaw=Buffer.concat([json])
  }
  // 有raw
  else if(raw){
    mixJsonRaw=Buffer.concat([zeroBuffer,raw])
  }
  // 没有json也没有raw
  else {
    mixJsonRaw=Buffer.allocUnsafe(0)
  }
  return mixJsonRaw
};
function buffer2jr(buffer){
  if(!etools.typeMatch(buffer,'buffer')) throw new Error("input raw isNot Buffer");

  let json,raw;
  // 解析过程
  const arry0=etools.bufFind0(buffer);

  if(buffer.length===0){
    // 不用做任何处理
  } else if(arry0.length===0){
    // 没有0位分割，全是json
    json=buffer
  } else if(arry0[0]===0){
    // 0位分割在0位，表示无json，只有raw
    raw=buffer.slice(1)
  } else {
    // 有0位分割，且前后均有数据
    json=(buffer.slice(0,arry0[0]));
    raw=buffer.slice(arry0[0]+1)
  }
  // json实例化
  if(json){
    json=json.toString('utf8');
    try{
      json=JSON.parse(json);
    } catch (e){
      json=null;
    }
  } else {
    json=null;
  }

  return [json,raw]
};

/* ====== 明文bufMsg消息处理 ====== */
// 构建bufMsg
function bufMsgBuild(buffer,isPacked,code,rNum){
  /* 输入校验 */
  // buffer类别
  if(!Buffer.isBuffer(buffer)) throw new Error('buffer is not Buffer');
  // code必须是0～255之间
  if(!etools.isInt(code)||code<0||code>255) throw new Error('code Error');
  // isPacked必须是boolean型
  if(typeof isPacked!=='boolean') throw new Error('isPacked must be true/false');
  // 如果未指定随机值，自行生成
  if(typeof rNum==='undefined') rNum=Number.parseInt(Math.random()*65536);
  if(!etools.isInt(rNum)||rNum<0||rNum>65535) throw new Error('randomNum Error');

  /* 构建消息头 */
  const head=Buffer.allocUnsafeSlow(8);
  // 0~3  长度位
  head.writeUInt32LE(buffer.length);
  // 4    fiveCode，打包类别位。未打包即明文，取0；打包时取code值
  head[4]=isPacked?code:0;
  // 5    sixCode，明文下有效的消息格式代码。明文时取code值，密文时此位无用，取随机值
  head[5]=isPacked?rNum>>8:code;
  // 6    随机位
  head[6]=rNum%256;
  // 7    校验码位，调用函数计算
  head[7]=bufMsgGetCheckNum(head);

  // 合并head和buffer，即为输出的bufMsg
  return Buffer.concat([head,buffer]);
}
// 校验码计算，可传入headBuf或bufMsg
function bufMsgGetCheckNum(headBuf) {
  if(!Buffer.isBuffer(headBuf)) throw new Error('headBuf is not Buffer');
  if(headBuf.length<8) throw new Error('headBuf length not enough');
  // 计算校验码
  let checkNum=0;
  for(let i=0;i<7;i++) checkNum+=headBuf[i]*(i+1);
  // 返回
  return checkNum%256;
}
// 检验bufMsg是否符合规范
function bufMsgCheck(bufMsg) {
  // 来校验的若不是Buffer，则是编程错误
  if(!Buffer.isBuffer(bufMsg)) throw new Error('bufBuf is not Buffer');
  // 长度必须大于等于8位
  if(bufMsg.length<8) return 'lengthLess8';
  // 校验码比对
  if(bufMsgGetCheckNum(bufMsg)!==bufMsg[7]) return 'checkNumError';
  // 长度比对
  if((bufMsg.readUInt32LE(0)+8)!==bufMsg.length) return 'lengthError';
  // 没有问题
  return
}

/* ====== 公用基类 ====== */
// 二进制流片段缓存，应对分包粘包问题
/*
* 实例化时，需跟int参数限定最大允许消息体长度
*
* 接收：调用方法bufPieceIn传入
*
* 输出：
*   重新定义bufMsgFunc(bufMsg)方法，接收整理好的标准bufMsg
*   重新定义errFunc(errcode)方法，errcode可能列表如下：
*     overMaxLen    - 超出允许最大长度
*
* 触发errFunc或bufMsgFunc后，均会实例均会复位，可继续使用
* */
class c_fluentCache {
  constructor(maxLen){
    // 如果没有定义最大长度，报错
    if(!etools.isInt(maxLen)||maxLen<1||maxLen>429e7) throw new Error('maxLen Error');
    // 最大允许长度
    this.maxLen=maxLen
    // 头4位长度缓存，始终复用
    this.head=Buffer.allocUnsafe(4);
    // 对外接口函数，需在外部替换
    this.errFunc=(errcode)=>{} // 错误触发函数
    this.bufMsgFunc=(bufMsg)=>{} // 切开的消息体

    // 初次调用复位方法
    this.reSet();
  }
  // 复位
  reSet(){
    // 当前接收指针，p<4时，长度位未完整；p>4时，接收到body
    this.p=0
    // 消息体积缓存，长度位接收完毕时创建
    this.msgBuf=null
  }
  // 二进制流片段进入
  bufPieceIn(bufPiece){
    // 验证输入，不是Buffer对象则报错
    if(!Buffer.isBuffer(bufPiece)) throw new Error('bufPiece is not Buffer');

    /* ========== 将二进制片段分析赋值到类内部缓存 ========= */
    for(let i=0;i<bufPiece.length;i++) {
      // 当长度位读取完毕时，构建缓存
      if(this.p===4) {
        // 消息体长度
        const bodyLen=this.head.readUInt32LE();
        // 超过允许最大长度，抛出错误
        if(bodyLen>this.maxLen){
          this.reSet(); // 复位
          this.errFunc('overMaxLen'); // 抛出错误
          break; // 退出循环
        }
        // 未超过，准备接收消息体
        else {
          // 构建消息缓存
          this.msgBuf=Buffer.allocUnsafe(8+bodyLen); // 前置码一共8位，前4位是长度位
          // 将长度位写入
          for(let i=0;i<4;i++) this.msgBuf[i]=this.head[i];
        }
      }
      

      // p<4时，读取长度位
      if(this.p<4) {
        this.head[this.p]=bufPiece[i];
        this.p++; // 指针位后移
      }
      // p>=4时，读取消息体
      else {
        this.msgBuf[this.p]=bufPiece[i];
        this.p++; // 指针位后移

        // 如果已经接收到最后，触发消息，并复位
        if(this.p===this.msgBuf.length) {
          this.bufMsgFunc(this.msgBuf); // 触发
          this.reSet(); // 复位
        }
      }
    }
  }
}

// bufMsg打包解包事务处理
/*
* 功能：
*   输入的包必须是单条标准bufMsg
*   解压缩或解密后的流，可能存在粘包，不可能存在分包
*
* 方法：
*   bufMsgIn(bufMsg)：接收底层数据，可接收任意单体bufMsg(明文、压缩、加密)
*   bufMsgSend(bufMsg，callback)：发送明文bufMsg，其回调函数仅有errcode项，可能列表如下：
*     lengthLess8    - bufMsg长度不足8位
*     lengthError    - bufMsg长度校验错误
*     checkNumError  - bufMsg校验码错误
*     zipError      - 压缩错误
*     cipherError   - 加密错误
*     socketError   - 发送执行错误
*
* 需要重定义的接口函数：
*   重新定义bufMsgFunc(bufMsg)方法，接收整理好的明文
*   重新定义errFunc(errcode)方法，自身运行中的错误，errcode可能列表如下：
*     lengthLess8    - bufMsg长度不足8位
*     lengthError    - bufMsg长度校验错误
*     unknowFiveCode - 不能识别的编码格式
*     checkNumError  - bufMsg校验码错误
*     bufMsgCutError - 分包错误
*     unZipError     - 解压缩错误
*     certError      - 解密时缺少证书
*     skeyError      - 解密时缺少会话密钥
*     decipherError  - 解密运算错误
*
* 触发errFunc时，会导致对应输入的bufMsg条目丢失
*
* */
class c_packIssue{
  constructor(){
    // 对外接口函数，需在外部替换
    this.errFunc=(errcode)=>{} // 错误触发函数
    this.bufMsgFunc=(bufMsg)=>{} // 明文消息体
    this.bufSend=null; // 打包后发送流的出口

    // 初次重置
    this.reSet(true);
  }
  // 重置
  reSet(isFirst){
    this.cert=null;      // 证书
    this.skey=null;      // 会话密钥
    this.zipMin=null;    // 压缩区间下限
    this.zipMax=null;    // 压缩区间上限
    this.recCache=[];    // 接收消息缓存
    this.isRunRec=false; // 是否正在解析消息
    this.sendCache=[];   // 发送消息缓存
    this.isRunSend=false;// 是否正在打包发送消息
  }
  // 设置证书(也既之后发送的消息会被加密)，重置一次之后只能设定一次
  setCert(cert){
    // 检验是否重复设置
    if(this.cert) throw new Error('set Cert again');
    // 设定证书，验证格式
    else if(Buffer.isBuffer(cert)&&cert.length===96){
      // 未创建内存，则创建
      if(!this.cert) this.cert=Buffer.allocUnsafeSlow(96);
      // 复制证书
      cert.copy(this.cert);
    }
    // 不知道干啥
    else throw new Error('Set Cert Error')
  }
  // 设置会话密钥(也既之后发送的消息会被加密)，重置一次之后只能设定一次
  setSkey(skey){
    // 检验是否重复设置
    if(this.skey) throw new Error('set Skey again');
    // 设定证书，验证格式
    else if(Buffer.isBuffer(skey)&&skey.length===30){
      // 未创建内存，则创建
      if(!this.skey) this.skey=Buffer.allocUnsafeSlow(30);
      // 复制证书
      skey.copy(this.skey);
    }
    // 不知道要干啥
    else throw new Error('Set SessionKey Error')
  }
  // 设置压缩发送长度区间
  setZipRange(min,max){
    // 接收下限
    if(min===null) this.zipMin=null;
    else if(etools.isInt(min)&&min>=0){
      this.zipMin=min;
    }
    else throw new Error('min value Error');

    // 接收上限
    if(max===null) this.zipMax=null;
    else if(etools.isInt(max)&&max>=0){
      this.zipMax=max;
    }
    else throw new Error('max value Error');
  }
  // 从上层接收消息，进入时可能是明文、密文、压缩
  bufMsgIn(bufMsg){
    // 验证bufMsg
    let errcode=bufMsgCheck(bufMsg);
    if(errcode){
      this.errFunc(errcode);
      return;
    }

    // 将消息放在缓存队尾
    this.recCache.push(bufMsg);

    // 如果接收循环停滞，则启动
    if(!this.isRunRec){
      this.isRunRec=true;
      this._runRecBoot();
    }
  }
  // 发送消息，调用时应该是明文（需提前指定bufSend方法）
  bufMsgSend(bufMsg,callback){
    // callback为可选参数，补充默认方法
    if(!callback) callback=(err)=>{
      if(err)
        etools.log('[noCallback - bufMsgSend] err: '+err);
    }

    // 空调用一次bufSend方法，其返回值仅代表发送接口状态
    let errMsg=this.bufSend();
    if(errMsg){
      // 无论如何，都报socketError
      callback('socketError');
      return;
    }

    // 验证bufMsg
    let errcode=bufMsgCheck(bufMsg);
    if(errcode){
      callback(errcode);
      return;
    }

    // 将消息放在缓存队尾
    this.sendCache.push([bufMsg,callback]);

    // 如果发送循环停滞，则启动
    if(!this.isRunSend){
      this.isRunSend=true;
      this._runSendBoot();
    }
  }

  /* ====== 内部工作 ====== */
  // 消息接收
  _runRecBoot(){
    // 取出一条消息
    const item=this.recCache.shift();
    // 如果取出为空，结束
    if(!item){
      // 标记
      this.isRunRec=false;
      return;
    }
    // 调用消息包处理
    this._singleBufMsg(item,(err)=>{
      if(err) this.errFunc(err);
      // 迭代处分类理下一条
      this._runRecBoot();
    })
  }
  // 处理单条消息，可能：明文包、加密包、压缩包
  _singleBufMsg(bufMsg,callback){
    const fiveCode=bufMsg[4];
    // 明文包
    if(fiveCode===0){
      // 抛出执行，输出为sixCode和排除了前8位的消息流
      this.bufMsgFunc(bufMsg);
      // 反馈结束
      callback()
    }
    /* 1~15 解压算法 */
    // gzip
    else if(fiveCode===1){
      zlib.gunzip(bufMsg.slice(8),undefined,(err,newbuf)=>{
        // 解压缩错误
        if(err) callback('unZipError');
        // 成功，转移
        else this._cutBufMsg(newbuf,callback);
      });
    }
    /* 16～31 解密算法 */
    // aes-128-ecb
    else if(fiveCode===21){
      // 判断有无证书、会话密钥
      if(!this.cert) callback('certError');
      else if(!this.skey) callback('skeyError');
      // 解密过程
      else {
        // 加密用的key
        const key=Buffer.concat([this.cert,this.skey,bufMsg.slice(5,7)]); // 组成完整密钥

        const decipher = crypto.createDecipher('aes-128-ecb', key);
        let newbuf;
        try{
          const buf_update=decipher.update(bufMsg.slice(8));
          const buf_final =decipher.final();
          newbuf=Buffer.concat([buf_update,buf_final]);
        } catch (e) {
          callback('decipherError');
          return;
        }
        // 成功，转移
        this._cutBufMsg(newbuf,callback);
      }
    }
    // 不知道是啥包
    else {
      callback('unknowFiveCode')
    }
  }
  // 可能需分包的消息处理
  _cutBufMsg(bufMsg,callback){
    /* === 分包 === */
    // 输出数组
    const arry=[];
    // 指针位
    let p=0;
    // 循环
    while (p<bufMsg.length){
      // 提取长度
      const len=bufMsg.readUInt32LE(p)+8;
      // 如果长度不足，报错
      if(p+len>bufMsg.length){
        callback('bufMsgCutError');
        return;
      }
      // 将指定长度添加到数组
      arry.push(bufMsg.slice(p,p+len));
      // 指针后移
      p+=len;
    }

    /* === 调用 === */
    const itemDo=(count)=>{
      // 递归到最后，结束
      if(count>=arry.length){
        callback();
        return;
      }
      
      // 分离
      const itemBuf=arry[count];
      // 计算校验码
      let checkNum=0;
      for(let i=0;i<7;i++) checkNum+=itemBuf[i]*(i+1);
      checkNum=checkNum%256;
      // 校验码比对失败，则结束
      if(checkNum!==itemBuf[7]){
        callback('checkNumError'); // 抛出错误
        return; // 结束处理
      }
      
      // 调用单条处理
      this._singleBufMsg(arry[count],(err)=>{
        // 单条遇到错误，反馈
        if(err) callback(err);
        // 没有遇到错误，递归
        else itemDo(count+1);
      })
    }
    // 首次调用
    itemDo(0);
  }

  // 消息发送
  _runSendBoot(){
    // 取出一条待发送消息和回调函数[0] sendBuffer, [1] callback
    const item=this.sendCache.shift();
    // 如果取出为空，结束
    if(!item){
      // 标记
      this.isRunSend=false;
      return;
    }
    // 调用发送
    this._bufMsgSend(item[0],(err)=>{
      // 执行回调函数
      item[1](err);
      // 迭代处分类理下一条
      this._runSendBoot();
    })
  }
  // 发送buffer流（需提前指定bufSend方法）
  _bufMsgSend_zip(bufMsg,isCrypto,callback){
    // 对bufMsg进行压缩
    zlib.gzip(bufMsg,undefined,(err,gzipBuf)=>{
      if(err){
        callback('zipError')
        return
      }
      // 将压缩后的流添加bufMsg消息头，fiveCode为1代表gzip
      const newBufMsg=bufMsgBuild(gzipBuf,true,1);

      /* 操作导向，2种情况 */
      // 如果需要加密，调用加密方法，同时将回调函数带去
      if(isCrypto) this._bufMsgSend_crypto(newBufMsg,callback);
      // 不需要压缩和加密，直接发送
      else this._bufMsgSend_do(newBufMsg,callback);
    });
  }
  _bufMsgSend_crypto(bufMsg,callback){
    // 计算随机位
    const rNum=Number.parseInt(Math.random(65536));
    // 消息密位
    const rBuf=Buffer.allocUnsafe(2);
    rBuf[0]=rNum>>8;
    rBuf[1]=rNum%256;
    // 合成消息key
    const key=Buffer.concat([this.cert,this.skey,rBuf]);
    // 加密过程
    let newBuf;
    try{
      let cip = crypto.createCipher('aes-128-ecb', key);
      newBuf = Buffer.concat([cip.update(bufMsg),cip.final()]);
    } catch (e){
      // 遇到错误
      callback('cipherError');
      return;
    }

    // 将压缩后的流添加bufMsg消息头，fiveCode为21代表该压缩方法
    const newBufMsg=bufMsgBuild(newBuf,true,21,rNum);

    // 执行发送
    this._bufMsgSend_do(newBufMsg,callback);
  }
  _bufMsgSend_do(bufMsg,callback){
    const errmsg=this.bufSend(bufMsg);
    // 无论任何错误，对外报socketError
    if(errmsg) callback('socketError');
    // 发送成功
    else callback();
  }
  _bufMsgSend(bufMsg,callback){
    // 合压缩条件
    const isZip=
      etools.isInt(this.zipMax) &&
      etools.isInt(this.zipMin) &&
      bufMsg.length>this.zipMin &&
      bufMsg.length<this.zipMax;

    // 加密条件
    const isCrypto=
      Buffer.isBuffer(this.cert) &&
      Buffer.isBuffer(this.skey);


    /* 操作导向，3种情况 */
    // 如果需要压缩，调用压缩方法，同时将是否需要加密、回调函数带去
    if(isZip) this._bufMsgSend_zip(bufMsg,isCrypto,callback);
    // 如果需要加密，调用加密方法，同时将回调函数带去
    else if(isCrypto) this._bufMsgSend_crypto(bufMsg,callback);
    // 不需要压缩和加密，直接发送
    else this._bufMsgSend_do(bufMsg,callback);
  }
}

// bufMsg明文处理类
/* 调用bufMsgIn方法输入单条明文bufMsg
 * 需重新定义方法：
 * jrFunc(json,raw) // 两者不会都为空
 * cmdFunc(strArry) // 字符串数组
 * errFunc(errcode) 错误代码：
 *    unknowSixCode - 未识别的类别编码
 *
*/
class c_readBufMsg{
  constructor(){
    this.jrFunc=()=>{};
    this.cmdFunc=()=>{};
    this.errFunc=()=>{};
  }
  bufMsgIn(bufMsg){
    // 如果非明文，一定是编程错误
    if(bufMsg[4]!==0) throw new Error('class readBufMsg meet fiveCode not 0')

    const sixCode=bufMsg[5];
    const buf=bufMsg.slice(8);
    // cmd指令编码
    if(sixCode===0){
      const strArry=buffer2cmds(buf);
      // 数组长度不为0，触发
      if(strArry.length>0) this.cmdFunc(strArry);
    }
    // jr消息指令编码
    else if(sixCode===1){
      const [json,raw]=buffer2jr(buf);
      // json、raw不都为空，触发
      if(json||raw) this.jrFunc(json,raw);
    }
    // 未识别的sixCode
    else this.errFunc('unknowSixCode')
  }
}

// netSocket封装
class c_tcpBase extends EventEmitter{
  constructor(cacheLength){
    super();
    // 内部初始化
    this.reSet(true);

    // 明文bufMsg解析类
    this._readBufMsg=new c_readBufMsg();
    this._readBufMsg.jrFunc=(json,raw)=>{
      // 登录状态
      if(this.isLoged) this._normal_jr(json,raw);
      // 未登录状态
      else this._reg_jr(json,raw);
    }
    this._readBufMsg.cmdFunc=(strArry)=>{
      // 登录状态
      if(this.isLoged) this._normal_cmd(strArry);
      // 未登录状态
      else this._reg_cmd(strArry);
    }
    this._readBufMsg.errFunc=(code)=>{
      switch (errcode) {
        case 'unknowSixCode': this._emitError(401); break;
        default : throw new Error('unknow errcode: '+errcode);
      }
    }

    // 打包事务处理
    this._packIssue=new c_packIssue();
    this._packIssue.bufMsgFunc= bufMsg=>this._readBufMsg.bufMsgIn(bufMsg);
    this._packIssue.bufSend=(buf)=>{
      // 判定_socket是否可用
      if(this._stateCode!==1) return 'socketError';
      // 没有发送内容，实际是在检测是否可发送，反馈空表示状态正常
      if(typeof buf==='undefined') return;
      // 如果buf不是Buffer，报错
      if(!Buffer.isBuffer(buf)) throw new Error('buf isnot Buffer');
      // 执行发送
      this._socket.write(buf);
    }
    this._packIssue.errFunc=(code)=>{
      switch (errcode) {
        case 'lengthLess8':this._emitError(311);break;
        case 'lengthError':this._emitError(303);break;
        case 'unknowFiveCode': this._emitError(307); break;
        case 'checkNumError': this._emitError(302); break;
        case 'bufMsgCutError': this._emitError(301); break;
        case 'unZipError': this._emitError(306); break;
        case 'certError': this._emitError(304,'缺少证书'); break;
        case 'skeyError': this._emitError(304,'缺少会话key'); break;
        case 'decipherError': this._emitError(305); break;
        default : throw new Error('unknow errcode: '+errcode);
      }
    }

    // 分包粘包处理类实例构建
    this._fluentCache=new c_fluentCache(cacheLength||conf.maxOriginalPackLength);
    this._fluentCache.bufMsgFunc= bufMsg=>{
      // 在此判定fiveCode是否合乎加密要求
      if(this.fiveCode>=16 && this.fiveCode<32 && bufMsg[4]!==this.fiveCode) this._emitError(211);
      // 合乎要求，去解密
      else this._packIssue.bufMsgIn(bufMsg);
    }
    this._fluentCache.errFunc=(errcode)=>{
      switch (errcode) {
        case 'overMaxLen': this._emitError(210); break;
        default : throw new Error('unknow errcode: '+errcode);
      }
    }

    this._conf={}; // 配置缓存字典
    this.on('error',()=>{});// 防止error无监听时throw Error

    // net.socket实例
    this._socket=null;
    // 周期触发函数集
    this._circleFuncs=[];
    this._circleClock=setInterval(()=>{
      this._circleFuncs.forEach((r)=>{
        // 当前秒数递增
        r.n++;
        // 若超过触发周期
        if(r.n>=r.t){
          r.n=0; // 归零
          r.f(); // 触发
        }
      });
    },1000);
    this._circleInit();
  }

  reSet(isFirst){
    // 连接状态
    this._info=null;   // 连接信息缓存字典，null时标示未登录
    this._stateCode=0; // 连接状态 0 未连接; 1 正常 ; 2 连接中 ; 3 断线等待重连
    this._emptySec=0;

    // 错误缓存
    this._errObj="";
    this._errCode=0;

    /* ======= 如果是首次，则到此可结束 ======= */
    if(isFirst) return;
    this._fluentCache.reSet();// 分包粘包处理类重置
    this._packIssue.reSet();// 解压解密类重置
  }

  /* ====== 类维护函数 ====== */
  // 添加周期触发函数
  _circle(second,func){
    this._circleFuncs.push({
      n:0,      // 当前秒数
      t:second, // 周期秒数
      f:func    // 函数体
    })
  }
  // 周期函数实例
  _circleInit(){
    // 心跳、超时
    this._circle(1,()=>{
      // 非正常通讯状态，不进行该操作
      if(this._stateCode!==1||!this.isLoged) return;

      this._emptySec++;// 计时器累加
      if(this.debug) etools.log('emptySec: '+this._emptySec);
      const heartTime=this._info && this._info.heartTime||30;
      let   heartOut =this._info && this._info.heartOut ||10;
      // 兼容早期heartOut为超时时间，会比heartTime小的情况
      if(heartOut<heartTime) heartOut=heartTime+heartOut;

      if(this._emptySec===heartTime){
        this.send_cmd('_heart');
      }
      if(this._emptySec===heartOut){
        this.stop(442,'emptySec Timeout')
      }
    });
  }
  // 销毁实例
  destroy(){
    // 清除秒周期触发
    clearInterval(this._circleClock);
    // 清除net.socket实例
    this._destroySocket();
  }
  // 销毁socket实例
  _destroySocket(){
    if(this._socket){
      this._socket.destroy();
      this._socket=null;
    }
  }

  /* ====== 常规事件触发函数 ====== */
  _emitError(code,err){
    // 内部记录
    if(typeof code!=='undefined') this._errCode=code;
    if(typeof err !=='undefined') this._errObj=err;
    // 尝试通知对方关闭
    this.send_jr({_error:this._errCode,msg:this._errObj},()=>{});
    // 对外触发
    const showmsg=(typeof this._errObj==='string')?this._errObj:JSON.stringify(this._errObj);
    if(this.debug) etools.log(`[${this.showName} Error ${code}] `+showmsg);
    this.emit('error',this._errCode,this._errObj);
    // 销毁socket实例
    this._destroySocket();
    // 触发连接关闭函数
    this._socketCloseFunc(this._errCode,this._errObj);
  }
  _emitWarn(msg){
    if(this.debug) etools.log(`[${this.showName} Warn] `+msg);
    this.emit('warn',msg);
  }
  _emitMsg(msg){
    if(this.debug) etools.log(`[${this.showName} Msg] `+msg);
    this.emit('msg',msg);
  }

  /* ====== 认证状态消息处理 ====== */
  // 认证过程命令处理在外层类中定义
  _normal_cmd(cmds){
    if(this.debug) etools.log(`[${this.showName} rec_cmd] ${cmds[0]}`)
    switch (cmds[0]){
      // 心跳
      case '_heart':
        this.send_cmd('_heartBak');
        break;
      case '_heartBak':
        break;
      // jr消息反馈
      case '_jrMsgBak':
        // 识别序号
        const count=Number.parseInt(cmds[1]);
        // 查缓冲区是否存在
        if(this._jrRetryDict[count]){
          // 记录回调函数
          const fn=this._jrRetryDict[count].callback;
          // 删除缓冲区等待数据
          delete this._jrRetryDict[count];
          // 下一周期执行回调
          process.nextTick(()=>{fn()});
        }
        break;
      // 没有特征，对外触发事件
      default:
        this.emit('rec_cmd',cmds);
    }
  }
  _normal_jr(json,raw){
    if(this.debug) etools.log(`[${this.showName} rec_jr ] ${json.cmd}`);
    // 是否对方通知错误需关闭
    if('_error' in json){
      this._emitError(json._error,json.msg);
      return;
    }

    // 是否要求反馈（仅为了适应就版本econn.js，新版本已经不再有该机制）
    if('_jrRetryCount' in json){
      this.send_cmd(['_jrMsgBak',json._jrRetryCount]);
      delete json._jrRetryCount
    }
    // 对外触发事件
    this.emit('rec_jr',json,raw);
  }

  /* ====== 内部功能函数 ====== */
  // 设置socket实例
  _renewSocket(socket){
    // 记录实例
    this.reSet();
    this._socket=socket;

    // socket无用事件日志记录
    if(this.debug){
      socket.on('drain'  ,()=>{etools.log('undo socket event: drain')});
      socket.on('end'    ,()=>{socket.destroy()});
      socket.on('lookup' ,()=>{etools.log('undo socket event: lookup')});
      socket.on('timeout',()=>{etools.log('undo socket event: timeout')});
    }

    // socket有用事件
    socket.on('error' ,(err)=>{
      // socket的error和this的error事件定义不一样
      // 仅记录错误信息到this，后续socket的close事件会触到外部
      this._errCode=201;
      this._errObj=JSON.stringify(err);
    });
    socket.on('close' ,()=>{
      this._stateCode=3; // 状态置为断线
      this._info=null; // 登录信息置为空
      this._emitError(); // 提交错误，代码在error事件中已保存
    });
    socket.on('data'  ,(buf)=>{
      // 有关闭错误代码后，不再接收新数据
      if(this._errCode) return;
      // 空白时间归零
      this._emptySec=0;
      // 向分包粘包类输入数据
      this._fluentCache.bufPieceIn(buf);
    });
  }

  /* ====== 对外功能函数 ====== */
  // 更新配置项
  setConf(conf){
    if(typeof conf!=='object') return;
    for(let key in conf) this._conf[key]=conf[key];
  }
  // 发送cmd格式消息
  send_cmd(strArry,callback){
    // strArry格式整理
    if(typeof strArry==='string') strArry=[strArry];

    /* 格式校验，不通过抛错 */
    // 必须是数组
    if(!Array.isArray(strArry)) throw new Error('send_cmd: strArry must be Array');
    // 每项必须是字符串或数字
    for(let i=0;i<strArry.length;i++){
      // 数字项转字符串
      if(typeof strArry[i]==='number') strArry[i]=strArry[i]+"";
      // 到此必须是字符串
      if(typeof strArry[i]!=='string') throw new Error('send_cmd: strArry item must be string');
    }
    // callback可以没有，若有必须是function
    if(callback && (typeof callback!=='function')) throw new Error('send_cmd: callback not Function');

    /* 整理发送 */
    const bufMsg=bufMsgBuild(cmds2buffer(strArry),false,0);
    this._packIssue.bufMsgSend(bufMsg,callback);
  }
  // 发送jr消息
  send_jr(json,raw,callback){
    // 参数顺序调整
    if(typeof raw==='function'){
      callback=raw;
      raw=undefined;
    }

    /* 格式校验，不通过抛错 */
    // json非object则报错
    if(!etools.typeMatch(json,'object')) throw new Error('send_jr: json is not Object');
    // raw可以没有，若有则必须是Buffer
    if(raw && !Buffer.isBuffer(raw)) throw new Error('send_jr: raw typeError');
    // callback可以没有，若有必须是function
    if(callback && (typeof callback!=='function')) throw new Error('send_jr: callback not Function');

    /* 整理发送 */
    const bufMsg=bufMsgBuild(jr2buffer(json,raw),false,1);
    this._packIssue.bufMsgSend(bufMsg,callback);
  }

  // 停止连接
  stop(code,msg){
    // 如果有重连尝试，则取消
    if(this._reConnClock){
      clearTimeout(this._reConnClock)
    }

    // 状态置为未连接
    this._stateCode=0;
    this._errCode=code;
    this._errObj=msg;

    // 销毁当前底层连接eSocket实例
    this._destroySocket();
  }

  /* ====== 对外属性 ====== */
  get isLoged(){
    // 以有没有this._info节点判定是否登录
    return !!this._info;
  }
  get cid(){
    return this._info.cid
  }            // 位号标示
  get uuid(){
    return(this._info && this._info.uuid) || this._conf.uuid || null;
  }           // 主机标示
  get host(){
    if(this._socket) return this._socket.remoteAddress;
    else return null
  }           // 对方主机
  get port(){
    if(this._socket) return this._socket.remotePort;
    else return null
  }           // 对方端口
  get timezone(){
    return this._info && this._info.timezone || null;
  }       // 获取时区（废弃，不可靠，不建议用）
  get loginTime(){
    return this._info && this._info.loginTime || null;
  }      // 获取注册通过时间
  get showName(){
    // 显示日志用
    if(this.isServerSide) return `serSocket - ${this.host}:${this.port}`
    else return `tcpSocket - ${this.host}:${this.port}`
  }       // 显示名
  get family(){
    return this._socket?this._socket.remoteFamily:null;
  }         // IP版本
  get sockid(){
    return this._socket? etools.sockId(this.host,this.port):null;
  }         // 连接对方唯一序号
  get selfmac(){
    // 没有socket，localip则反馈空null
    if(!this._socket||!this._socket.localAddress) return null;

    const ip=this._socket.localAddress;
    const interfaces=os.networkInterfaces();

    // 遍历循环，查找配置
    for(let network in interfaces){
      for(let i=0;i<(interfaces[network]).length;i++){
        const inf=interfaces[network][i];
        if(inf.address===ip)
          return inf.mac
      }
    }

    // 未获取到，反馈空字符串
    return null
  }        // 己方MAC地址
  get fiveCode(){
    // 未登录时，用0。已登录，则用info节点中的值
    return this._info?this._info.fiveCode:0
  }

  /* 对外事件
  * error(code,err) 连接关闭
  *   0   正常关闭
  *   4   有新的uuid重复，被挤掉线
  *   0xx 外界输入的关闭代码(在外部调用close方法时参数输入)
  *   201 socket原生错误
  *   210 分包粘包时，数据包超长
  *   211 收到消息不符合加密要求
  *   301 解析后，分包错误
  *   302 解析后包头信息校验错误
  *   303 包长度溢出
  *   304 解加密包但没有证书或会话密钥
  *   305 解加密包错误
  *   306 解压缩包错误
  *   307 没能力处理的fiveCode
  *   308 输入的buf不是流
  *   309 压缩错误
  *   310 加密错误
  *   311 解析包时遇到不足8位的bufMsg
  *   401 解析时sixCode无法处理
  *   405 (客户端)读取本地证书错误
  *   406 (客户端)重写本地证书错误
  *   407 (客户端)从服务器端接收到的证书错误
  *   408 (客户端)从本地读取证书错误
  *   410 (服务器)接收注册信息错误
  *   411 收到原始消息与当前应有fiveCode不匹配
  *   440 登录信息被服务器拒绝
  *   441 登录阶段收到服务器不理解的消息
  *   442 心跳超时
  *   443 客户登录请求被regFunc要求转向
  *   446 regFunc函数反馈信息无法识别
  *   447 服务段更新证书失败，注册函数反馈的cert格式不正确
  *
  * rec_cmd/jr/custom 正常通讯状态收到以上类型消息
  * loged(regInfo) 通过服务器端认证
  * */
}

/* ====== 客户端Socket ====== */
class tcpSocket extends c_tcpBase{
  constructor(conf){
    super();
    this.setConf(conf);
  }

  // 未认证阶段，jr消息处理
  _reg_boot(){
    // 连接建立后，客户端首次发送登录验证信息
    const regObj={
      cmd:'_reg',
      uuid:this._conf.uuid||os.hostname(),
      mac:this.selfmac,
      hostname:os.hostname(),
      pkey:this._packIssue.skey.toString('hex')
    };
    // 尝试读取cert并进行md5加密
    let certMd5Buf;
    if(this._conf.certPath){
      try{
        // 记录本地存储的证书
        const cert=fs.readFileSync(this._conf.certPath);

        // 检验证书
        if(cert.length!==96){
          this._emitError(408);
        } else {
          // 暂存该证书，但不能更新到分包粘包的实例中
          this._certTemp=cert;
          // 证书形式有效，生成md5码以便服务器端验证
          certMd5Buf=etools.md5(cert);
        }
      } catch(err){
        /* 读取证书错误不影响认证过程
        *  err.code:
        *  ENOENT - 没有该文件或文件夹
        *  EACCES - 没有权限
        * */
        this._emitError(405,err.code);
      }
    }

    // 发送认证信息
    this.send_jr(regObj,certMd5Buf);

    // 调试日志
    if(this.debug){
      etools.log(`[DEBUG] ~~ client regMsg ~~`);
      etools.log(`[DEBUG] json: ${JSON.stringify(regObj)}`);
      if(certMd5Buf){
        etools.log(`[DEBUG] cert MD5: ${certMd5Buf.toString('hex')}`);
      }
      etools.log(`[DEBUG] ~~~~~~~~~~~~~~~~~~~`);
    }
  }
  _reg_jr(regBak,cert){
    // 是否对方通知错误需关闭
    if('_error' in regBak){
      this._emitError(regBak._error,regBak.msg);
      return;
    }
    // 登录成功
    if(regBak.cmd==='_regInfo'){
      if(this.debug) etools.log('[DEBUG] loginfo: '+JSON.stringify(regBak));
      if(this.debug) etools.log('[DEBUG] log with cert Renew: '+!!cert);

      this._info=regBak;             // 更新配置项目
      if(this.debug) etools.log('Client Loged');
      // 要求加密
      if(this.fiveCode>=16&&this.fiveCode<32){
        // 要求更新证书
        if(cert){
          // 检查证书
          if(!Buffer.isBuffer(cert)||cert.length!==96){
            this._emitError(407,'登录服务器发来证书长度不对');
            return;
          }
          // 更新内存
          this._certTemp=cert;
          // 如果客户端准备存储证书，则尝试写入
          if(etools.typeMatch(this._certPath,'string')){
            fs.writeFile(this._certPath,cert,(err)=>{
              if(err){
                this._emitWarn('406 证书写入本地磁盘错误');
              }
            })
          }
        }

        // 将证书更新到包处理类
        this._packIssue.setCert(this._certTemp);
      }

      // 触发外部类loged事件
      this.emit('loged',regBak);
    }
    // 未理解的消息
    else {
      // 类内尚无错误信息，则以输入为准记录
      if(!this._errCode){
        this._errCode=441;
        this._errObj=regBak;
      }
      // 断开连接
      this.stop();
    }
  }

  // 尝试连接
  _tryConn(reHost,rePort){
    // 在状态2、3时，可调用该方法
    if(this._stateCode!==0&&this._stateCode!==3){
      this._emitWarn('只有在stateCode为0或3时，可调用_tryConn方法');
      return
    }
    // 状态置为尝试连接
    this._stateCode=2;

    // 仍有_socket 报错
    if(this._socket){
      this._emitWarn('_socket实例仍然存在，无法执行_tryConn方法');
      return
    }

    // 新建socket连接
    const socket=new net.Socket();
    this._renewSocket(socket);
    // 若有指定的转向host、port，优先级高于conf配置
    socket.connect(rePort||this._conf.port,reHost||this._conf.host,()=>{
      // 置为正常连接状态
      this._stateCode=1;
      // 生成会话key
      this._packIssue.setSkey(crypto.randomBytes(30));
      // 连接成功后，触发
      this._reg_boot();
    });
  }

  // 遇到错误关闭，或转向连接
  _socketCloseFunc(errCode,err){
    /* ======== 确定重连等待时间 ======== */
    // -1不重连，0立即，正整数为等待秒数。
    let sleep_second=99;// 默认值
    // 根据关闭的错误代码，决定重连间隔时间
    switch (errCode){
      // 转向连接，立即
      case 443:
        this._emitMsg(`Redirect to: ${err.host}:${err.port}`);
        // 如果重连计时器已存在，先清除之前的
        if(this._reConnClock) clearTimeout(this._reConnClock);
        // 立即重连
        this._tryConn(err.host,err.port);
        return;
        break;
      case 0  :sleep_second=10 ; break;// 服务器端正常关闭，立即
      case 4  :sleep_second=600; break;// 被挤掉线，10分钟后尝试
      case 100:sleep_second=100; break;// 服务器端未启动
      case 440:sleep_second=600; break;// 登录被服务器拒绝
      case 441:sleep_second=600; break;// 服务器端反馈不理解
      case 442:sleep_second=0  ; break;// 心跳超时，立即重连
      case 446:sleep_second=600; break;// 登录函数反馈不识别
      default:
        // 没有被case到，范围处理
        // 不是自然数，非正常到此
        if(!etools.isInt(errCode)||errCode<0){
        }
        // 外界调用关闭
        else if(errCode>=0&&errCode<100){
          sleep_second=60;
        }
        // 内部_socket错误
        else if(errCode>=100&&errCode<200){
          sleep_second=20;
        }
        // 内部_fluent错误
        else if(errCode>=200&&errCode<300){
          sleep_second=10;
        }
        break
    }
    // 若是手动调用stop方法，则_stateCode已置为0，不再重连
    if(this._stateCode===0) sleep_second=-1;

    // 重连间隔为负数，则不重连
    if(sleep_second<0){
      // 状态置为未连接(手动stop时，可能已处于0，重新赋值也无妨)
      this._stateCode=0;
      this.emit('close',errCode,err);
    }
    // 为正数，进入等待重连状态
    else {
      // 如果重连计时器已存在，先清除之前的
      if(this._reConnClock) clearTimeout(this._reConnClock);
      // 设定重连倒计时器
      this._reConnClock=setTimeout(()=>{
        this._tryConn();
      },sleep_second*1000);
      // 写重连日志
      etools.log(`[${this.showName}] ReConn after ${sleep_second} second`);
    }
  }

  // 启动连接
  start(){
    // 只有在未连接状态，可以调用启动函数
    if(this._stateCode!==0){
      this._emitWarn('只有在stateCode为0时，可以调用start方法');
      return
    }

    // 尝试连接
    this._tryConn();
  }

  // 对外属性
  get isServerSide(){return false}
  get _certPath(){
    return this._conf.certPath;
  } // 证书路径
}
/* ====== 服务端Socket ====== */
class c_serSocket extends c_tcpBase{
  constructor(socket,conf){
    super();
    this.setConf(conf);
    this._renewSocket(socket);
    this._stateCode=1;// 服务端stateCode建立时即为1
  }

  // 未认证阶段，jr消息处理
  _reg_jr(regObj,certMd5Buf){
    // 是否对方通知错误需关闭
    if('_error' in regObj){
      this._emitError(regObj._error,regObj.msg);
      return;
    }
    /* [regObj] 用户段传来的登录信息
     *  cmd     : '_reg'
     *  uuid    : 客户端提供的唯一标示
     *  mac     : 当前通讯网卡的mac
     *  hostname: 计算机名
     *  pkey    : 会话随机码
     *  [certMd5Buf]
     *  客户端cert经过md5编码后的流
     */
    // 检查eServer已经定义认证函数，出错则为编程错误
    if(typeof this._conf.regFunc!=='function')
      throw new Error('tcpServer donot defined regFunc');

    // 此时只能接收cmd为_reg
    if(regObj.cmd!=='_reg'){
      this._emitError(410,'客户端注册命令错误');
      return;
    }

    // 接收会话加密码pkey，并从regObj中删掉
    if(regObj.pkey&&(typeof regObj.pkey==='string')&&regObj.pkey.length===60){
      try{
        this._packIssue.setSkey(Buffer.from(regObj.pkey, 'hex'))
      }catch(e){
        this._emitError(410,'客户端会话pkey错误');
        return;
      }
      delete regObj.pkey;
    }

    // 如果regObj中没有uuid，则以hostname+_+mac作为uuid
    if (!regObj.uuid) regObj.uuid = regObj.hostname + '_' + regObj.mac;

    // 调用用户server代码中定义的regFunc
    this._conf.regFunc(regObj,certMd5Buf,(err,regBak,cert)=>{
      // regObj 是客户端发来的请求
      // regBak 是regFunc认证过程中生成的json
      // ========= 错误处理 =========
      if(err) {
        this._emitError(440,err);
      }
      // ========= 认证成功 =========
      else if(regBak&&regBak.cmd==='_regInfo')
      {
        regBak.loginTime = Date.now(); // 登录时间戳

        // 丰富_info节点内容
        regBak.fiveCode    = this._conf.fiveCode;
        regBak.minZipBytes = this._conf.minZipBytes;
        regBak.maxZipBytes = this._conf.maxZipBytes;
        regBak.heartTime   = this._conf.heartTime;
        regBak.timeOut     = this._conf.timeOut;

        // 向客户端发送时，cmd修改
        regBak.cmd='_regInfo';

        // 向客户端发送认证信息
        if(regBak.isInit){
          // 初始化发送认证obj和证书
          delete regBak.isInit;
          this.send_jr(regBak,cert)
        } else {
          // 非初始化仅发送认证obj
          this.send_jr(regBak)
        }

        // 更新服务器端serSocket状态，即_info字段
        /* ！此处必须注意要等上面向客户端发送的消息发出后，才可改变服务器状态。
         * 否则，会造成客户端收到加密消息，但客户端尚不能解密。
         * ！ */
        this._info=regBak;

        // 触发服务器端socket的loged事件
        this.emit('loged');
        if(this.debug) etools.log('Server Loged');

        // 加密通讯，需更新证书实例
        if(this.fiveCode>=16&&this.fiveCode<32){
          // 验证证书形式合格性
          if(Buffer.isBuffer(cert)&&cert.length===96){
            // 合法，更新到实例
            this._packIssue.setCert(cert);  // 记录证书用于加解密
          }
          else{
            // 不合格，记录日志并关闭连接
            this._emitError(447,'regFunc函数反馈的证书不合格');
          }
        }
      }
      // ========= 转向登录 =========
      else if(regBak&&regBak.cmd==='_redirect')
      {
        // 以特定关闭代码443关闭，客户端即可识别转向登录
        this._emitError(443,regBak);
      }
      // ========= 未知命令，挂掉程序 =========
      else {
        this._emitError(446,'regFunc函数callback返回值');
      }
    });
  }

  // 遇到错误关闭，或转向连接
  _socketCloseFunc(errCode,err){};

  // 对外属性
  get isServerSide(){return true}
}
/* ====== 服务端监听 ====== */
// 明文直接通过的注册认证函数
function default_regFunc(regObj,cert,callback){
  regObj.cmd='_regInfo';
  regObj.isCrypto=false;
  callback(undefined,regObj)
}
class tcpServer extends EventEmitter{
  constructor(conf){
    super();
    // 初始化配置项内容
    this._conf={
      listenPort:null,
      ipv4Mask:null,
      ipv6Mask:null,
      fiveCode:null,
      minZipBytes:null,
      maxZipBytes:null,
      debug:false,
      heartTime:null,
      timeOut:null
    };
    this.setConf(conf);
  }

  // 新连接建立后处理方法
  _newServerSocket(socket){
    // ip地址过滤规则
    let ipRawLocal,ipRawRemote;
    try{
      ipRawLocal=etools.ip2raw(socket.localAddress);
      ipRawRemote=etools.ip2raw(socket.remoteAddress);
    } catch (e) {
      // 获取ip地址失败
      etools.log('[econn new serSocker] 获取serSocket的IP地址失败: local-'+socket.localAddress+' remote-'+socket.remoteAddress);
      socket.destroy();
      return; // bug 20171203
    }
    const mask=(socket.remoteAddress.indexOf('.')>0?this._conf.ipv4Mask+96:this._conf.ipv6Mask)/8;
    for(let i=0;i<mask;i++){
      if(ipRawLocal[i]!==ipRawRemote[i]){
        this.emit('error',2002);
        socket.destroy();
        return
      }
    }

    // 生成server端eSocket对象
    const serSocket=new c_serSocket(socket,{
      heartTime:this._conf.heartTime,
      timeOut:this._conf.timeOut,
      minZipBytes:this._conf.minZipBytes,
      maxZipBytes:this._conf.maxZipBytes,
      regFunc:this.regFunc||default_regFunc,
      fiveCode:this._conf.fiveCode,
      debug:this._conf.debug
    });
    serSocket.debug=this.debug; // 传递调试选项

    // 触发tcpServer类的事件
    this.emit('connection',serSocket)
  }

  // 更新配置项
  setConf(conf){
    if(typeof conf!=='object') return;
    for(let key in this._conf){
      if(key in conf) this._conf[key]=conf[key];
    }
  }
  // 启动监听
  _boot(){
    this._server=net.createServer({
      allowHalfOpen: false,
      pauseOnConnect: false
    });
    this._server.on('connection',(socket)=>{
      this.emit('msg',this.showName+' clientFrom: '+socket.remoteAddress+':'+socket.remotePort);
      this._newServerSocket(socket)
    });
    this._server.on('listening',()=>{
      // 兼容老版本，对外触发Start事件。
      this.emit('msg',this.showName+' Starting');
      this.emit('start')
    });

    // 确定可能监听的端口范围
    let portStart,portEnd,portNow;
    // 区分单端口和范围端口设置
    if(Array.isArray(this._conf.listenPort)){
      portStart=this._conf.listenPort[0];
      portEnd  =this._conf.listenPort[this._conf.listenPort.length-1];
    } else {
      portStart=portEnd=this._conf.listenPort
    }
    // 尝试监听方法
    const tryListen=(server,port)=>{
      server.listen(port)
    };
    // 监听错误处理方法
    const that=this;
    this._server.on('error',(err)=>{
      portNow++;
      // 已经尝试到超过允许监听范围，对外报错
      if(portNow>portEnd){
        that.emit('close',2001,err)
      }
      // 尝试监听范围内下一端口
      else {
        tryListen(that._server,portNow)
      }

    });
    // 启动初次监听
    portNow=portStart;
    tryListen(this._server,portNow)


  }
  start(){
    // 先检测当前是否有监听，若有则报警告，关闭后再连接
    if(this._server){
      const that=this;

      // this.emit('error',2201);
      this._server.close(()=>{
        that._server=null;
        that._boot();
      })
    } else {
      this._boot();
    }
  }
  destroy(){
    // 若有监听，则先关闭
    if(this._server){
      this._server.close(()=>{
        this._server=null;
      });
    }
    this.removeAllListeners(this.eventNames());
  }

  // 属性
  get showName(){return `[econn tcpServer - listenPort ${this.listenPort}]`}

  get listenPort(){
    const addr=this._server?this._server.address():null;
    return addr?addr.port:undefined
  }

  /*
  * 对外事件：
  * close(closeCode[,err]) 关闭时带出的错误代码
  * 2001：内部net.socket报错
  *
  * error(errorCode[,err]) 警告性的不影响运行的错误代码
  * 2201：start方法时之前监听未停止
  * */
}

// 依赖于jr消息的API机制
class c_apiCore {
  constructor() {
    // 调试开关，默认为关闭
    this.debug=false;
    // 发送jr消息函数
    this.sendJrFunc = null;
    // 对外提供的API字典集合（高优先级）
    this._apiDict = {}
    // 对外提供的API正则匹配数组（低优先级，有顺序）
    this._apiRegex = []
    // 对外调用count计数器
    this._count = 0;
    // 己方对外请求的缓存
    this._reqCache = {
      // key 发出时己方生成的count
      // value 数组四项：[sec, json, raw, callback]
    };
    // 对方请求，己方运行完毕的结果缓存
    this._resCache = {
      // key 对方请求的count
      // value 数组三项：[sec, json, raw]
      // 其中，json项为null，则表明己方接收到请求但尚未处理完毕；为object时，表明已经处理完毕
    };
    // 内部秒循环
    this._secCircle =setInterval(()=>this._secCircleFunc(),1000);
    // 是否已被销毁
    this._destroyed=false;
  }
  // 秒循环工作函数
  _secCircleFunc(){
    /* 缓存定时清理 */
    // 响应结果保存15秒，超过时间删除
    for(const count in this._resCache){
      // 获取发送对象
      const resObj=this._resCache[count];
      // 计时器累加
      resObj[0]++;
      // 超时删除
      if(resObj[0]>=15){
        // 调试输出
        if (this.debug) etools.log('[DEBUG] API-RES 缓存到期，删除，count: '+count);

        delete this._resCache[count];
      }
    }

    // 请求收到反馈时立即删除，未收到时在缓存里
    for(const count in this._reqCache){
      // 获取发送对象
      const reqObj=this._reqCache[count];
      // 计时器累加
      reqObj[0]++;
      // 第5、10秒重新发送
      if(reqObj[0]===5||reqObj[0]===10){
        this.sendJrFunc(reqObj[1],reqObj[2]);

        // 调试输出
        if (this.debug) etools.log('[DEBUG] API-REQ 重发请求，count: '+count);
      }
      // 超过15秒删除，执行回调函数
      if(reqObj[0]>=15){
        delete this._reqCache[count]; // 从字典中删除
        reqObj[3]('timeout'); // 执行回调函数，报超时错误

        // 调试输出
        if (this.debug) etools.log('[DEBUG] API-REQ 请求超时，count: '+count);
      }
    }
  }

  // 根据API字符串匹配调用方法
  _apiFunc(api){
    // api必须是字符串，否则为编程错误
    if(typeof api!=='string') throw new Error('api must be String');

    // 优先级 1：如果有字典匹配，直接反馈
    if(api in this._apiDict) return this._apiDict[api];
    
    // 优先级 2：依次尝试正则匹配，匹配成功，则反馈
    for(let i=0;i<this._apiRegex.length;i++)
      if(this._apiRegex[i][0].test(api)) return this._apiRegex[i][1]
    
    // 末次：返回未找到方法
    return ((info,post, raw, callback)=>{callback('noApi: ' + json.api)})
  }

  // 尝试处理jr消息的入口，能够成功处理，则反馈true；不属于api机制，则反馈false
  isJrMatch(json, raw) {
    // 如果已经销毁，不再接收jr消息
    if(this._destroyed) return false;
    // json是否是对象，不是则反馈false
    if (!etools.typeMatch(json, 'object')) return false;
    // count项必须是自然数，否则不符合API机制
    if (!Number.isSafeInteger(json.count) || json.count < 0) return false;

    // cmd是api，调用己方API
    if (json.cmd === 'api') {
      /* 根据count判断该从次请求要怎么处理 */
      const resCache = this._resCache[json.count];

      // 新请求，缓存字典中无记录，着手处理该请求
      if(!resCache){
        // 调试输出
        if (this.debug) etools.log('[DEBUG] API-RES 新请求，建立缓存，执行，count: '+json.count);
        // 首先在resCache字典中，建立记录
        const resCache = this._resCache[json.count]=[0,null,null];
        
        // 构造info对象
        const info={
          api:json.api // 调用的API名称
        }
        
        // 匹配方法，并调用，调用处理
        this._apiFunc(json.api)(info,json.reqJson, raw, (err, resJson, raw) => {
          // 生成发送的json
          const sendJson = {
            cmd: 'apiBak',
            err: err,
            resJson: resJson,
            count: json.count
          }

          // 将反馈结果存入缓存
          resCache[1]=sendJson;
          resCache[2]=raw;


          // 调试输出
          if (this.debug) etools.log('[DEBUG] API-RES 处理完毕，填充缓存，初次发送，count: '+json.count);

          // 将处理结果发送出去
          this.sendJrFunc(sendJson, raw);
        })
      }
      // 已经接受到请求，且已经处理完毕，再次发送处理结果
      else if (resCache[1]){
        // 调试输出
        if (this.debug) etools.log('[DEBUG] API-RES 重复请求，已处理完毕，再次发送，count: '+json.count);

        this.sendJrFunc(resCache[1], resCache[2]);
      }
      // 已经接受到请求，但尚未处理完毕，不用任何处理
      else {
        // 调试输出
        if (this.debug) etools.log('[DEBUG] API-RES 重复请求，尚未处理完毕，不响应，count: '+json.count);
      }

      // 结束，表明符合API机制
      return true;
    }
    // cmd是apiBak，己方调用的反馈
    else if (json.cmd === 'apiBak') {
      // 从缓存取出请求对象
      const reqObj=this._reqCache[json.count];
      // 取出成功
      if(reqObj){
        // 从缓存中删除该记录
        delete this._reqCache[json.count];
        // 执行回调函数
        reqObj[3](json.err,json.resJson,raw);
        // 调试输出
        if (this.debug) etools.log('[DEBUG] API-REQ 请求响应成功，count: '+json.count);
      }
      // 取出失败意味着已经超时，不用管了

      // 结束，表明符合API机制
      return true;
    }
    // 未识别cmd，不属于API机制
    else return false;
  }

  // 调用对方API的方法
  callApi(api, reqJson, raw, callback) {
    // 获取序号
    const count = this._count;
    if (count < 214e7) this._count++; // 计数器累加
    else this._count = 0; // 越界归零

    // 组织请求体
    const json = {
      cmd: 'api',
      api, reqJson, count
    }

    // 缓存请求
    this._reqCache[count] = [0, json, raw, callback];

    // 发送请求
    this.sendJrFunc(json, raw);

    // 调试日志
    if(this.debug) etools.log('[DEBUG] API-REQ 发出新请求，count: '+count);
  }
  
  // 设定对外提供的API
  regApi(api, func) {
    // 根据api类型，来确定设定方法
    switch (etools.etype(api)){
      // 字符串：添加字典匹配方法
      case 'string':
        // 转为字典形式
        const obj = {};
        obj[api] = func;
        api = obj;
        // 没有break，紧接着进入字典处理方式
        
      // 字典：key为api名，value为处理方法，依次添加到内部字典
      case 'dict':
        // 判定api必须为字典，否则为编程错误
        if (etools.etype(api)!=='dict') throw new Error('apiName format Error');

        // 遍历字典
        for (const key in api) {
          const func = api[key];
          // 验证func必须是函数，否则是编程错误
          if (etools.etype(func)!== 'function') throw new Error('func format Error');
          // 内部登记
          this._apiDict[key] = func;
        }
        break;
        
      // 正则对象，添加到正则匹配数组
      case 'regex':
        // 包装成数组，不Break
        api=[[api,func]]

      // 数组：有序多个正则匹配方法
      case 'array':
        // 遍历数组添加
        for(let i=0;i<api.length;i++){
          const item=api[i];
          // 验证api项目必须是正则对象
          if(etools.etype(item[0])!=='regex') throw new Error('regex format Error');
          // 验证func必须是函数，否则是编程错误
          if (etools.etype(item[1])!== 'function') throw new Error('func format Error');
          // 验证成功，添加到内部字典
          this._apiRegex.push(item);
        }
        break

      // 未识别
      default: throw new Error('unknow how to regApi');
    }
  }

  // 销毁
  destroy() {
    // 标记已经销毁
    this._destroyed=true;
    // 清空定时器
    clearInterval(this._secCircle);
  }
}

/* ====== 对外接口 ====== */
module.exports={
  fluentCache:c_fluentCache,
  tcpServer:tcpServer,
  tcpSocket:tcpSocket,
  apiCore:c_apiCore,
};
